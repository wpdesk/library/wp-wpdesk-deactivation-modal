
## [1.5.5] - 2024-11-19
- Support for WordPress 6.7
- Updated libs

## [1.5.3] - 2023-06-13
- Upgrade wp-wpdesk-tracker to the latest version

## [1.0.0] - 2021-09-07
### Added
- First version
