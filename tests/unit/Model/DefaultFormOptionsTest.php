<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Model\DefaultFormOptions;

class DefaultFormOptionsTest extends TestCase {

	public function test_get_options() {
		$object = new DefaultFormOptions();

		$this->assertEquals( $object->get_options()[0]->get_key(), 'plugin_stopped_working' );
		$this->assertEquals( $object->get_options()[1]->get_key(), 'broke_my_site' );
		$this->assertEquals( $object->get_options()[2]->get_key(), 'found_better_plugin' );
		$this->assertEquals( $object->get_options()[3]->get_key(), 'plugin_for_short_period' );
		$this->assertEquals( $object->get_options()[4]->get_key(), 'no_longer_need' );
		$this->assertEquals( $object->get_options()[5]->get_key(), 'temporary_deactivation' );
		$this->assertEquals( $object->get_options()[6]->get_key(), 'other' );
	}
}
