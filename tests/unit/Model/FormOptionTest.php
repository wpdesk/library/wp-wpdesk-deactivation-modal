<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Model\FormOption;

class FormOptionTest extends TestCase {

	public function test_get_key() {
		$value  = 'field_name';
		$object = new FormOption( $value, 10, '', null, null );

		$this->assertEquals( $object->get_key(), $value );
	}

	public function test_get_priority() {
		$value  = 10;
		$object = new FormOption( '', $value, '', null, null );

		$this->assertEquals( $object->get_priority(), $value );
	}

	public function test_set_priority() {
		$value_before = 10;
		$value_after  = 20;

		$object = new FormOption( '', $value_before, '', null, null );
		$object->set_priority( $value_after );

		$this->assertEquals( $object->get_priority(), $value_after );
	}

	public function test_get_label() {
		$value  = 'Field label';
		$object = new FormOption( '', 10, $value, null, null );

		$this->assertEquals( $object->get_label(), $value );
	}

	public function test_set_label() {
		$value_before = 'Label before';
		$value_after  = 'Label after';

		$object = new FormOption( '', 10, $value_before, null, null );
		$object->set_label( $value_after );

		$this->assertEquals( $object->get_label(), $value_after );
	}

	public function test_get_default_message() {
		$object = new FormOption( '', 10, '' );

		$this->assertEquals( $object->get_message(), null );
	}

	public function test_get_message() {
		$value  = 'Lorem ipsum...';
		$object = new FormOption( '', 10, '', $value, null );

		$this->assertEquals( $object->get_message(), $value );
	}

	public function test_set_message() {
		$value_before = 'Message before';
		$value_after  = 'Message after';

		$object = new FormOption( '', 10, '', $value_before, null );
		$object->set_message( $value_after );

		$this->assertEquals( $object->get_message(), $value_after );
	}

	public function test_get_default_question() {
		$object = new FormOption( '', 10, '' );

		$this->assertEquals( $object->get_question(), null );
	}

	public function test_get_question() {
		$value  = 'Lorem ipsum?';
		$object = new FormOption( '', 10, '', null, $value );

		$this->assertEquals( $object->get_question(), $value );
	}

	public function test_set_question() {
		$value_before = 'Question before';
		$value_after  = 'Question after';

		$object = new FormOption( '', 10, '', null, $value_before );
		$object->set_question( $value_after );

		$this->assertEquals( $object->get_question(), $value_after );
	}
}
