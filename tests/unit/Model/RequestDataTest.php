<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Model\RequestData;

class RequestDataTest extends TestCase {

	public function test_plugin_slug() {
		$value  = 'plugin_slug';
		$object = new RequestData( $value );

		$this->assertEquals( $object->get_plugin_slug(), $value );
	}

	public function test_set_reason_key() {
		$value  = 'reason_key';
		$object = ( new RequestData( 'plugin_slug' ) )
			->set_reason_key( $value );

		$this->assertEquals( $object->get_reason_key(), $value );
	}

	public function test_set_additional_info() {
		$value  = 'additional_info';
		$object = ( new RequestData( 'plugin_slug' ) )
			->set_additional_info( $value );

		$this->assertEquals( $object->get_additional_info(), $value );
	}

	public function test_set_additional_data() {
		$value  = [
			'value_1' => 'Value 1',
			'value_2' => 'Value 2',
		];
		$object = ( new RequestData( 'plugin_slug' ) )
			->set_additional_data( $value );

		$this->assertEquals( $object->get_additional_data(), $value );
	}

	public function test_set_additional_data_item() {
		$data_key_1   = 'value_1';
		$data_value_1 = 'Value 1';
		$data_key_2   = 'value_2';
		$data_value_2 = 'Value 2';
		$object       = ( new RequestData( 'plugin_slug' ) )
			->set_additional_data_item( $data_key_1, $data_value_1 )
			->set_additional_data_item( $data_key_2, $data_value_2 );

		$additional_data = $object->get_additional_data();
		$this->assertEquals( $additional_data[ $data_key_1 ], $data_value_1 );
		$this->assertEquals( $additional_data[ $data_key_2 ], $data_value_2 );
	}
}
