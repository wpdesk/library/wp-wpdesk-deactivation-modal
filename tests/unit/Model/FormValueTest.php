<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Model\FormValue;

class FormValueTest extends TestCase {

	public function test_get_key() {
		$value  = 'value_key';
		$object = new FormValue(
			$value,
			function () {
			}
		);

		$this->assertEquals( $object->get_key(), $value );
	}

	public function test_get_value_callback() {
		$value          = 'value';
		$callback_value = function () use ( $value ) {
			return $value;
		};
		$object         = new FormValue( '', $callback_value );

		$this->assertEquals(
			call_user_func( $object->get_value_callback() ),
			$value
		);
	}
}
