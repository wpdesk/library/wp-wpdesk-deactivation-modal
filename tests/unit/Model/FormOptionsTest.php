<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Exception\DuplicatedFormOptionKeyException;
use WPDesk\DeactivationModal\Exception\ReservedFormOptionKeyException;
use WPDesk\DeactivationModal\Exception\UnknownFormOptionKeyException;
use WPDesk\DeactivationModal\Model\FormOption;
use WPDesk\DeactivationModal\Model\FormOptions;

class FormOptionsTest extends TestCase {

	public function test_set_option() {
		$option_key = 'option_key';
		$object     = ( new FormOptions() )
			->set_option( new FormOption( $option_key, 10, '' ) );

		$this->assertEquals( $object->get_options()[0]->get_key(), $option_key );
	}

	public function test_set_duplicated_option_key() {
		$this->expectException( DuplicatedFormOptionKeyException::class );

		$option_key = 'option_key';
		( new FormOptions() )
			->set_option( new FormOption( $option_key, 10, '' ) )
			->set_option( new FormOption( $option_key, 10, '' ) );
	}

	public function test_set_reserved_option_key() {
		$this->expectException( ReservedFormOptionKeyException::class );

		$option_key = 'null';
		( new FormOptions() )
			->set_option( new FormOption( $option_key, 10, '' ) );
	}

	public function test_delete_option() {
		$option_key = 'option_key';
		$object     = ( new FormOptions() )
			->set_option( new FormOption( $option_key, 10, '' ) )
			->delete_option( $option_key );

		$this->assertEmpty( $object->get_options() );
	}

	public function test_delete_unknown_option() {
		$this->expectException( UnknownFormOptionKeyException::class );

		( new FormOptions() )
			->set_option( new FormOption( 'option_1', 10, '' ) )
			->delete_option( 'option_2' );
	}

	public function test_update_option() {
		$option_key   = 'option_key';
		$label_before = 'Option label 1';
		$label_after  = 'Option label 2';

		$object = ( new FormOptions() )
			->set_option( new FormOption( $option_key, 10, $label_before ) )
			->update_option(
				$option_key,
				function ( FormOption $option ) use ( $label_after ) {
					$option->set_label( $label_after );
				}
			);

		$this->assertEquals( $object->get_options()[0]->get_label(), $label_after );
	}

	public function test_update_unknown_option() {
		$this->expectException( UnknownFormOptionKeyException::class );

		( new FormOptions() )
			->set_option( new FormOption( 'option_1', 10, '' ) )
			->update_option(
				'option_2',
				function ( FormOption $option ) {
					$option->set_label( 'New label' );
				}
			);
	}

	public function test_get_sorted_options() {
		$option_key_1 = 'option_key_1';
		$option_key_2 = 'option_key_2';
		$option_key_3 = 'option_key_3';

		$object = ( new FormOptions() )
			->set_option( new FormOption( $option_key_1, 20, '' ) )
			->set_option( new FormOption( $option_key_2, 30, '' ) )
			->set_option( new FormOption( $option_key_3, 10, '' ) );

		$this->assertEquals( $object->get_options()[0]->get_key(), $option_key_3 );
		$this->assertEquals( $object->get_options()[1]->get_key(), $option_key_1 );
		$this->assertEquals( $object->get_options()[2]->get_key(), $option_key_2 );
	}
}
