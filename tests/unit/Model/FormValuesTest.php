<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Exception\DuplicatedFormValueKeyException;
use WPDesk\DeactivationModal\Model\FormValue;
use WPDesk\DeactivationModal\Model\FormValues;

class FormValuesTest extends TestCase {

	public function test_set_value() {
		$value_key = 'value_key';
		$object    = ( new FormValues() )
			->set_value( new FormValue( $value_key, function () {
			} ) );

		$this->assertEquals( $object->get_values()[0]->get_key(), $value_key );
	}

	public function test_set_duplicated_value() {
		$this->expectException( DuplicatedFormValueKeyException::class );

		$value_key = 'value_key';
		( new FormValues() )
			->set_value(
				new FormValue(
					$value_key,
					function () {
					}
				)
			)
			->set_value(
				new FormValue(
					$value_key,
					function () {
					}
				)
			);
	}
}
