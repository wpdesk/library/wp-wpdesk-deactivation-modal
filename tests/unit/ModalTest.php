<?php

use PHPUnit\Framework\TestCase;
use WPDesk\DeactivationModal\Modal;
use WPDesk\DeactivationModal\Model\DefaultFormOptions;
use WPDesk\DeactivationModal\Model\FormTemplate;
use WPDesk\DeactivationModal\Model\FormValues;
use WPDesk\DeactivationModal\Sender\DataWpdeskSender;

class ModalTest extends TestCase {

	public function test_get_options() {
		new Modal(
			'plugin-slug',
			( new FormTemplate( 'Plugin name' ) ),
			( new DefaultFormOptions() ),
			( new FormValues() ),
			new DataWpdeskSender(
				'plugin-name/plugin-name.php',
				'Plugin name'
			)
		);

		$this->assertTrue( true );
	}
}
