<?php

use WP_Mock\Tools\TestCase;
use WPDesk\DeactivationModal\Exception\SenderRequestFailedException;
use WPDesk\DeactivationModal\Model\RequestData;
use WPDesk\DeactivationModal\Sender\DataWpdeskSender;

class DataWpdeskSenderTest extends TestCase {

	public function test_generate_request_data() {
		$plugin_slug      = 'plugin-name';
		$reason_key       = 'reason_1';
		$additional_info  = 'Reason message';
		$additional_data  = [
			'value_1' => 'Value 1',
		];
		$plugin_file_name = 'plugin-name/plugin-name.php';
		$plugin_name      = 'Plugin name';

		$request_data = ( new RequestData( $plugin_slug ) )
			->set_reason_key( $reason_key )
			->set_additional_info( $additional_info )
			->set_additional_data( $additional_data );
		$request_body = ( new DataWpdeskSender( $plugin_file_name, $plugin_name ) )
			->generate_request_data( $request_data );

		$this->assertEquals( $request_body['click_action'], 'plugin_deactivation' );
		$this->assertEquals( $request_body['plugin'], $plugin_file_name );
		$this->assertEquals( $request_body['plugin_name'], $plugin_name );
		$this->assertEquals( $request_body['reason'], $reason_key );
		$this->assertEquals( $request_body['additional_info'], $additional_info );
		$this->assertEquals( $request_body['additional_data'], $additional_data );
	}

	public function test_generate_request_data_without_reason() {
		$request_data = new RequestData( 'plugin-name' );
		$request_body = ( new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' ) )
			->generate_request_data( $request_data );

		$this->assertEquals( $request_body['reason'], 'null' );
	}

	public function test_generate_request_data_without_additional_info() {
		$request_data = new RequestData( 'plugin-name' );
		$request_body = ( new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' ) )
			->generate_request_data( $request_data );

		$this->assertArrayNotHasKey( 'additional_info', $request_body );
	}

	public function test_send_request() {
		\WP_Mock::userFunction( 'wp_remote_post', [
			'return' => [],
		] );

		$request_status = ( new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' ) )
			->send_request( new RequestData( 'plugin-name' ) );

		$this->assertTrue( $request_status );
	}

	public function test_send_failed_request() {
		$this->expectException( SenderRequestFailedException::class );

		\WP_Mock::userFunction( 'wp_remote_post', [
			'return' => $this->createMock( 'WP_Error' ),
		] );
		( new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' ) )
			->send_request( new RequestData( 'plugin-name' ) );
	}
}
