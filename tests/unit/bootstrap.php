<?php

require_once __DIR__ . '/../../vendor/autoload.php';

error_reporting( E_ALL );

WP_Mock::setUsePatchwork( true );
WP_Mock::bootstrap();

if ( ! class_exists( 'WP_Error' ) ) {
	class WP_Error {

		public function get_error_code() {
			return null;
		}

		public function get_error_message() {
			return null;
		}
	}
}
