<?php

use WP_Mock\Tools\TestCase;
use WPDesk\DeactivationModal\Service\AssetsPrinterService;

class AssetsPrinterServiceTest extends TestCase {

	public function test_hooks() {
		$object = new AssetsPrinterService( 'custom-plugin-name' );

		\WP_Mock::expectActionAdded( 'admin_print_styles-plugins.php', [ $object, 'load_styles' ] );
		\WP_Mock::expectActionAdded( 'admin_print_footer_scripts-plugins.php', [ $object, 'load_scripts' ] );

		$object->hooks();
		$this->assertHooksAdded();
	}

	public function test_print_css_code() {
		$plugin_slug = 'custom-plugin-name';

		$this->expectOutputRegex( '/(<style)(.*)("' . $plugin_slug . '")(.*)(<\/style>)/' );
		( new AssetsPrinterService( $plugin_slug ) )->load_styles();
	}

	public function test_print_js_code() {
		$plugin_slug = 'custom-plugin-name';

		$this->expectOutputRegex( '/(<script)(.*)("' . $plugin_slug . '")(.*)(<\/script>)/' );
		( new AssetsPrinterService( $plugin_slug ) )->load_scripts();
	}
}
