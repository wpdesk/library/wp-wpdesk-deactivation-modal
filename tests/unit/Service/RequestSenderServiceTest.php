<?php

use WP_Mock\Tools\TestCase;
use WPDesk\DeactivationModal\Model\FormOption;
use WPDesk\DeactivationModal\Model\FormOptions;
use WPDesk\DeactivationModal\Model\FormValue;
use WPDesk\DeactivationModal\Model\FormValues;
use WPDesk\DeactivationModal\Sender\DataWpdeskSender;
use WPDesk\DeactivationModal\Service\RequestSenderService;

class RequestSenderServiceTest extends TestCase {

	public function test_hooks() {
		$plugin_slug = 'custom-plugin-slug';
		$hook_suffix = RequestSenderService::ADMIN_AJAX_ACTION . $plugin_slug;
		$object      = new RequestSenderService(
			$plugin_slug,
			new FormOptions(),
			new FormValues(),
			new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' )
		);

		\WP_Mock::expectActionAdded( 'wp_ajax_' . $hook_suffix, [ $object, 'handle_ajax_request' ] );
		\WP_Mock::expectActionAdded( 'wp_ajax_nopriv_' . $hook_suffix, [ $object, 'handle_ajax_request' ] );

		$object->hooks();
		$this->assertHooksAdded();
	}

	public function test_generate_ajax_url() {
		$plugin_slug = 'custom-plugin-slug';
		$nonce_value = 'nonce-value';

		\WP_Mock::userFunction( 'wp_create_nonce', [
			'return' => $nonce_value,
		] );

		$this->assertEquals(
			RequestSenderService::generate_ajax_url( $plugin_slug ),
			sprintf(
				'admin-ajax.php?action=wpdesk_deactivation_request_%1$s&nonce=%2$s',
				$plugin_slug,
				$nonce_value
			)
		);
	}

	public function test_handle_ajax_request() {
		\WP_Mock::userFunction( 'wp_verify_nonce', [
			'return' => 1,
		] );
		\WP_Mock::userFunction( 'wp_remote_post', [
			'return' => [],
		] );
		\WP_Mock::userFunction( 'wp_die' );
		\WP_Mock::userFunction( 'wp_send_json_success' )->once();
		\WP_Mock::passthruFunction( 'wp_unslash' );

		$reason_key = 'reason_1';
		( new RequestSenderService(
			'custom-plugin-slug',
			( new FormOptions() )
				->set_option( new FormOption( $reason_key, 10, '' ) ),
			( new FormValues() )
				->set_value(
					new FormValue(
						'value_1',
						function () {
							return 'Value 1';
						}
					)
				),
			new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' )
		) )->handle_ajax_request( [
			'_deactivation_reason' => $reason_key,
		] );

		$this->assertTrue( true );
	}

	public function test_handle_ajax_request_with_invalid_reason_key() {
		\WP_Mock::userFunction( 'wp_verify_nonce', [
			'return' => 1,
		] );
		\WP_Mock::userFunction( 'wp_remote_post', [
			'return' => [],
		] );
		\WP_Mock::userFunction( 'wp_die' );
		\WP_Mock::userFunction( 'wp_send_json_success' )->once();
		\WP_Mock::passthruFunction( 'wp_unslash' );

		$reason_key = 'reason_1';
		( new RequestSenderService(
			'custom-plugin-slug',
			( new FormOptions() )
				->set_option( new FormOption( $reason_key, 10, '' ) ),
			( new FormValues() )
				->set_value(
					new FormValue(
						'value_1',
						function () {
							return 'Value 1';
						}
					)
				),
			new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' )
		) )->handle_ajax_request( [
			'_deactivation_reason' => 'reason_2',
		] );

		$this->assertTrue( true );
	}

	public function test_handle_ajax_unauthorized_request() {
		\WP_Mock::userFunction( 'wp_verify_nonce', [
			'return' => false,
		] );
		\WP_Mock::userFunction( 'wp_remote_post' );
		\WP_Mock::userFunction( 'wp_die' );
		\WP_Mock::userFunction( 'wp_send_json_error' )->once();
		\WP_Mock::passthruFunction( 'wp_unslash' );

		( new RequestSenderService(
			'custom-plugin-slug',
			new FormOptions(),
			new FormValues(),
			new DataWpdeskSender( 'plugin-name/plugin-name.php', 'Plugin name' )
		) )->handle_ajax_request();

		$this->assertTrue( true );
	}
}
