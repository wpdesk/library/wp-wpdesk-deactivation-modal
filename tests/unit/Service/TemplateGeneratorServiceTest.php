<?php

use WP_Mock\Tools\TestCase;
use WPDesk\DeactivationModal\Model\FormOption;
use WPDesk\DeactivationModal\Model\FormOptions;
use WPDesk\DeactivationModal\Model\FormTemplate;
use WPDesk\DeactivationModal\Model\FormValues;
use WPDesk\DeactivationModal\Service\TemplateGeneratorService;

class TemplateGeneratorServiceTest extends TestCase {

	public function test_hooks() {
		$object = new TemplateGeneratorService( '', new FormTemplate( 'Plugin name' ), new FormOptions(), new FormValues() );

		\WP_Mock::expectActionAdded( 'admin_print_footer_scripts-plugins.php', [ $object, 'load_template' ], 0 );

		$object->hooks();
		$this->assertHooksAdded();
	}

	public function test_print_template() {
		\WP_Mock::userFunction( 'wp_create_nonce', [
			'return' => 'nonce_value',
		] );
		\WP_Mock::passthruFunction( 'wp_kses_post' );

		$modal_title     = 'custom_modal_title';
		$form_option_key = 'reason_key_custom';

		$this->expectOutputRegex( '/(' . $modal_title . ')/' );
		$this->expectOutputRegex( '/(<input type="radio")(.*)(value="' . $form_option_key . '")/' );

		( new TemplateGeneratorService(
			'',
			new FormTemplate( 'Plugin name' ),
			( new FormOptions() )
				->set_option( new FormOption( $form_option_key, 10, '' ) ),
			new FormValues()
		) )->load_template();
	}
}
