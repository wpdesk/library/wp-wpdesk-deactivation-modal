### Installation:

```
composer require wpdesk/wp-wpdesk-deactivation-modal
```

### Default usage:

```
new WPDesk\DeactivationModal\Modal(
	'plugin-slug',
	( new WPDesk\DeactivationModal\Model\FormTemplate( 'Plugin name' ) ),
	( new WPDesk\DeactivationModal\Model\DefaultFormOptions() ),
	( new WPDesk\DeactivationModal\Model\FormValues() ),
	new WPDesk\DeactivationModal\Sender\DataWpdeskSender(
		'plugin-name/plugin-name.php',
		'Plugin name'
	)
);
```

### Modifications of the list of reasons for plugin deactivation:

```
new WPDesk\DeactivationModal\Modal(
	'plugin-slug',
	( new WPDesk\DeactivationModal\Model\FormTemplate( 'Plugin name' ) ),
	( new WPDesk\DeactivationModal\Model\DefaultFormOptions() )
		->delete_option( 'reason_1' )
		->update_option(
			'reason_2',
			function ( WPDesk\DeactivationModal\Model\FormOption $option ) {
				$option->set_label( 'Reason 2 - new label' );
			}
		)
		->set_option( new WPDesk\DeactivationModal\Model\FormOption(
			'reason_4',
			11,
			__( 'Reason 4', 'text-domain' )
		) ),
	( new WPDesk\DeactivationModal\Model\FormValues() ),
	new WPDesk\DeactivationModal\Sender\DataWpdeskSender(
		'plugin-name/plugin-name.php',
		'Plugin name'
	)
);
```

### Custom list of reasons for plugin deactivation:

```
new WPDesk\DeactivationModal\Modal(
	'plugin-slug',
	( new WPDesk\DeactivationModal\Model\FormTemplate( 'Plugin name' ) ),
	( new WPDesk\DeactivationModal\Model\FormOptions() )
		->set_option( new WPDesk\DeactivationModal\Model\FormOption(
			'reason_1',
			10,
			__( 'Reason 1', 'text-domain' ),
			__( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'text-domain' ),
			__( 'Aliquam ut placerat metus?', 'text-domain' )
		) )
		->set_option( new WPDesk\DeactivationModal\Model\FormOption(
			'reason_2',
			20,
			__( 'Reason 2', 'text-domain' )
		) )
		->set_option( new WPDesk\DeactivationModal\Model\FormOption(
			'reason_3',
			30,
			__( 'Reason 3', 'text-domain' ),
			null,
			__( 'Aliquam ut placerat metus?', 'text-domain' )
		) ),
	( new WPDesk\DeactivationModal\Model\FormValues() ),
	new WPDesk\DeactivationModal\Sender\DataWpdeskSender(
		'plugin-name/plugin-name.php',
		'Plugin name'
	)
);
```

### Additional values sent in the request reporting plugin deactivation:

```
new WPDesk\DeactivationModal\Modal(
	'plugin-slug',
	( new WPDesk\DeactivationModal\Model\FormTemplate( 'Plugin name' ) ),
	( new WPDesk\DeactivationModal\Model\DefaultFormOptions() ),
	( new WPDesk\DeactivationModal\Model\FormValues() )
		->set_value( new WPDesk\DeactivationModal\Model\FormValue(
			'extra_value_1',
			function () {
				return 'value_1';
			}
		) )
		->set_value( new WPDesk\DeactivationModal\Model\FormValue(
			'extra_value_2',
			function () {
				return 'value_2';
			}
		) ),
	new WPDesk\DeactivationModal\Sender\DataWpdeskSender(
		'plugin-name/plugin-name.php',
		'Plugin name'
	)
);
```

### Modifications of the modal template:

```
new WPDesk\DeactivationModal\Modal(
	'plugin-slug',
	( new WPDesk\DeactivationModal\Model\FormTemplate( 'Plugin name' ) )
		->set_form_title( __( 'Aliquam ut placerat metus', 'text-domain' ) )
		->set_form_desc( __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit:', 'text-domain' ) ),
	( new WPDesk\DeactivationModal\Model\DefaultFormOptions() ),
	( new WPDesk\DeactivationModal\Model\FormValues() ),
	new WPDesk\DeactivationModal\Sender\DataWpdeskSender(
		'plugin-name/plugin-name.php',
		'Plugin name'
	)
);
```
